# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.2] - mai 15, 2022
### Added
- :sparkles: 15º challenge implemented

## [1.0.2] - mai 14, 2022
### Added
- :sparkles: started implementation of medium challenges

## [1.0.1] - mai 10, 2022
### Added
- :sparkles: all easy challenges have been implemented
- :sparkles: 14º challenge implemented
- :sparkles: 13º challenge implemented
- :sparkles: 12º challenge implemented
- :sparkles: 11º challenge implemented
- :sparkles: 10º challenge implemented
- :sparkles: 9º challenge implemented
- :sparkles: 8º challenge implemented
- :sparkles: 7º challenge implemented
- :sparkles: started implementation of easy challenges

## [1.0.0] - mai 09, 2022
### Added
- :sparkles: all very easy challenges have been implemented
- :sparkles: 6º challenge implemented
- :sparkles: 5º challenge implemented
- :sparkles: 4º challenge implemented
- :sparkles: 3º challenge implemented
- :sparkles: 2º challenge implemented
- :sparkles: 1º challenge implemented
- :tada: docker initial implementation