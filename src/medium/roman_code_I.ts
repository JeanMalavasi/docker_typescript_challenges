function romanCalculus(romanNumber: string) {

    const romanArray = romanNumber
        .split('')

    let result = 0

    for (let index = 0; index < romanArray.length; index += 2) {
        const before = romanArray[index] as RomanNumbersType
        const after = romanArray[index + 1] as RomanNumbersType
        const afterAfter = romanArray[index + 2] as RomanNumbersType

        if (before === after && after === afterAfter) {
            result += parseInt(RomanNumbers[after]) * 3
            index++
        } else {
            if (after) {
                if (RomanNumbers[before] < RomanNumbers[after]) {
                    result += parseInt(RomanNumbers[after]) - parseInt(RomanNumbers[before])
                } else {
                    result += parseInt(RomanNumbers[before]) + parseInt(RomanNumbers[after])
                }
            } else {
                result += parseInt(RomanNumbers[before])
            }
        }
    }
    return result;
}

enum RomanNumbers {
    I = 1,
    V = 5,
    X = 10,
    L = 50,
    C = 100,
    D = 500,
    M = 1000,
}

type RomanNumbersType = '1' | '5' | '10' | '50' | '100' | '500' | '1000'

const entries: string[] = [
    'XLVII'
    , 'CDXXXVIII'
    , 'CMIX'
    ,'MMMCMXCIX'
    ,'CCCLXXXVII'
]

console.group(__filename.toUpperCase().split(/(\\|\/)/g).pop())
entries.forEach(romanNumber => {
    console.log(romanCalculus(romanNumber));
})
console.log('\n');
console.groupEnd()

export default {}