function factorial(number: number) {
    let factorialResult = BigInt(number)

    if (number <= 1)
        return factorialResult

    factorialResult = factorialResult * factorial(number - 1)

    return factorialResult
}

const entries: number[] = [5, 0, 32, 9]

console.group(__filename.toUpperCase().split(/(\\|\/)/g).pop())
entries.forEach(number => {
    console.log(factorial(number));
})
console.log('\n');
console.groupEnd()

export default {}