function validateCode(code: number) {
    const codeList = code
        .toString()
        .split('')

    const pair = codeList
        .reduce((previusValue, currentValue, currentIndex) => (
            currentIndex % 2 === 0 ?
                previusValue + currentValue :
                previusValue
        ), 0) as number * 3

    const odd = codeList
        .reduce((previusValue, currentValue, currentIndex) => (
            currentIndex % 2 !== 0 ?
                previusValue + currentValue :
                previusValue
        ), 0) as number * 1


    return (odd + pair) % 10 === 0 ?
        0 :
        10 - (odd + pair) % 10
}

const entries: number[] = [
    547020743789
    , 301354030348
    , 301354030349
    , 123456789872
]

console.group(__filename.toUpperCase().split(/(\\|\/)/g).pop())
entries.forEach(code => {
    console.log(validateCode(code));
})
console.log('\n');
console.groupEnd()

export default {}