function coordinateRange(coordinates: number[]) {
    const [x, y] = coordinates
    const response = []

    for (let repetX = 0; repetX <= x; repetX++) {
       for (let repetY = 0; repetY <= y; repetY++) {
            response.push([repetX, repetY])           
       }        
    }

    return response;
    
}

const entries: number[][] = [
    [2,2]
    ,[2,7]
    ,[-3,-3]
    ,[7,6]
]

console.group(__filename.toUpperCase().split(/(\\|\/)/g).pop())
entries.forEach(coordinates => {
    console.log(coordinateRange(coordinates));
})
console.log('\n');
console.groupEnd()

export default {}