function coinsCount(price: number) {
    let coinsList = {
        '1': 0,
        '5': 0,
        '10': 0,
        '25': 0,
        '100': 0,
        '500': 0
    }
    
    Object
        .keys(coinsList)
        .reverse()
        .forEach((coin: '500' | '100' | '25' | '10' | '5' | '1')  => {
            while(price - parseInt(coin) >= 0) {
                coinsList[coin] += 1
                price -= parseInt(coin)
            }
        })

    return coinsList
}

const entries: number[] = [
    478
    , 384
    , 5412
    , 50
]

console.group(__filename.toUpperCase().split(/(\\|\/)/g).pop())
entries.forEach(price => {
    console.log(coinsCount(price));
})
console.log('\n');
console.groupEnd()

export default {}