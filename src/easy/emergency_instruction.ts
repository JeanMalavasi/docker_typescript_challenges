function keyValidation(key: string) {
    let lettersCount: { [key: string]: number } = {}

    key
        .replace(/\s/g, '')
        .split('')
        .forEach(letter => {
            lettersCount.hasOwnProperty(letter) ?
                lettersCount[letter] += 1 :
                lettersCount[letter] = 1
        })

    return Object
        .values(lettersCount)
        .some(letterTotal => letterTotal % 2 !== 0) ?
        false :
        true

}

const entries: string[] = [
    'This is Thee'
    , 'ssd'
    , 'Lorem ipsum'
    , 'QQwweerrttyy'
]

console.group(__filename.toUpperCase().split(/(\\|\/)/g).pop())
entries.forEach(key => {
    console.log(keyValidation(key));
})
console.log('\n');
console.groupEnd()

export default {}