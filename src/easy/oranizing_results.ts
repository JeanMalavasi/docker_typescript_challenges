function organizingResults(inspectionResult: number[][]) {
    return inspectionResult
        .flat()
        .sort((previusValue, nextValue) => (
            previusValue - nextValue
        ))      
}

const entries: number[][][] = [
    [[1, 5, 3], [], [47, 128, 5], [1, 93, 57, 42, 103]]
    ,[[1, 3], [4, 8], [7, 5], [2, 6]]
    ,[[], [], [], []]
    ,[[100, 50], [60, 100], [20, 100, 70], [10, 40, 80, 90]]
]

console.group(__filename.toUpperCase().split(/(\\|\/)/g).pop())
entries.forEach(inspectionResult => {
    console.log(organizingResults(inspectionResult));
})
console.log('\n');
console.groupEnd()

export default {} 