abstract class License {
    private _name: string
    private _surname: string
    private _birth: Date

    constructor(name: string, surname: string, birth: Date) {
        this._name = name,
            this._surname = surname,
            this._birth = birth
    }

    get name() {
        return this._name
    }
    get surname() {
        return this._surname
    }
    get birth() {
        return this._birth
    }
}

class Pilot extends License {
    private _flyLicense: false | string = false

    constructor(name: string, surname: string, birth: Date) {
        super(name, surname, birth);
    }

    set flyLicense(license: string) {
        this._flyLicense = license
    }

    public createFlyLicense() {
        let construcLicense = this.surname.substring(0, 5).toUpperCase()
        while (construcLicense.length < 5) {
            construcLicense += 9
        }
        construcLicense =
            construcLicense.concat(
                '-',
                this.birth.getFullYear().toString()[2],
                this.birth.toISOString()[5],
                this.birth.toISOString()[6],
                this.birth.getUTCFullYear().toString()[3],
                '.',
                this.name[0].toLowerCase()
            )

        this.flyLicense = construcLicense
        return true
    }

}

const entries: { name: string, surname: string, birth: Date }[] = [
    {
        name: 'Jhon',
        surname: "Doe",
        birth: new Date(1977, 4, 25, 3, 0, 0, 0)
    },
    {
        name: 'Hal',
        surname: "Jordan",
        birth: new Date(1995, 8, 2, 3, 0, 0, 0)
    },
    {
        name: 'Carol',
        surname: "Danvers",
        birth: new Date(1968, 7, 17, 3, 0, 0, 0)
    },
    {
        name: 'Poe',
        surname: "Dameron",
        birth: new Date(1979, 2, 17, 3, 0, 0, 0)
    },
]

console.group(__filename.toUpperCase().split(/(\\|\/)/g).pop())

entries.forEach(pilot => {
    let license = new Pilot(pilot.name, pilot.surname, pilot.birth)
    license.createFlyLicense()
    console.log(license);

})


console.log('\n');
console.groupEnd()

export default {}