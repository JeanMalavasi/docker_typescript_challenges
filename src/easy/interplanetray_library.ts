function nameSimplify(authorName: string) {
    let nameSimplifyed = authorName
        .split(' ')

    let initalValue = nameSimplifyed.pop() + ', '

    return nameSimplifyed.reduce((previusValue, currentValue) => (
        previusValue + currentValue[0] + '. '
    ), initalValue).toUpperCase()
    
}

const entries: string[] = [
    'Isaac Larrumbia Ferreira Pontes'
    ,'John Ronald Reuel Tolkien'
    ,'Christopher james paolini'
    ,'Suzanne Marie Collins'
]

console.group(__filename.toUpperCase().split(/(\\|\/)/g).pop())
entries.forEach(authorName => {
    console.log(nameSimplify(authorName));
})
console.log('\n');
console.groupEnd()

export default {} 