function biggerCharacter(phrase: string) {
    return phrase.split('')
        .reduce((previusValue, currentValue) => (
            previusValue > currentValue ?
                previusValue :
                currentValue
        ), '')

}

const entries: string[] = [
    'Lorem ipsum dolores sec avanti',
    'Hello',
    'May the force be with you',
    'Its over nine thousand'
]

console.group(__filename.toUpperCase().split(/(\\|\/)/g).pop())
entries.forEach(phrase => {
    console.log(biggerCharacter(phrase));
})
console.log('\n');
console.groupEnd()

export default {}