function speedTravel(speedInitial: number) {
   
    return speedInitial.toString()
        .split("")
        .reduce((previusValue, currentValue) => (
            previusValue + Math.pow(parseInt(currentValue), 2)
        ), '')
}

const entries: number[] = [3514, 94571, 24, 745821698]

console.group(__filename.toUpperCase().split(/(\\|\/)/g).pop())
entries.forEach(speed => {
    console.log(speedTravel(speed));
})
console.log('\n');
console.groupEnd()

export default {}