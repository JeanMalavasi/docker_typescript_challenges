function reverseLenguage(language: any[]) {
    let newLanguage = []

    for (let index = language.length-1; index >= 0; index--) {
        newLanguage.push(language[index])
    }

    return newLanguage
}

const entries: any[][] = [
    [0,9,6,8,9,1,5,7],
    ['Oh', 'Hi', 'Mark'],
    [false, true, true, true],
    ["It's", "not", true, 0]
]

console.group(__filename.toUpperCase().split(/(\\|\/)/g).pop())
entries.forEach(language => {
    console.log(reverseLenguage(language));
})
console.log('\n');
console.groupEnd()

export default {}