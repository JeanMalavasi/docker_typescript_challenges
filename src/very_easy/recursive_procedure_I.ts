function recursiveChunks(numberChuncks: number) {
    let chunckString = 'chunk'

    if (numberChuncks <= 1)
        return chunckString
    chunckString = chunckString.concat('-', recursiveChunks(numberChuncks - 1))

    return chunckString
}

const entries: number[] = [4, 1, 8, 2]

console.group(__filename.toUpperCase().split(/(\\|\/)/g).pop())
entries.forEach(numberChuncks => {
    console.log(recursiveChunks(numberChuncks));
})
console.log('\n');
console.groupEnd()

export default {}