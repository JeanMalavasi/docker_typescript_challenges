function biggerCharacter(phrase: string) {
    return phrase.toLowerCase()
        .split(' ')
        .map(word => word.split('').reverse().join(''))
        .join(' ')
}

const entries: string[] = [
    'Lorem ipsum dolores sec avanti',
    'This is an apple',
    'May the force be with you',
    'It s over nine thousand'
]

console.group(__filename.toUpperCase().split(/(\\|\/)/g).pop())
entries.forEach(phrase => {
    console.log(biggerCharacter(phrase));
})
console.log('\n');
console.groupEnd()

export default {}