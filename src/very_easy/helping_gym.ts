function arithmeticAvarage(...groups: number[]) {
    const numberGroups = groups.length
    const numberStudents = groups.reduce((previusValue, currentValue) => (
        previusValue + currentValue
    ), 0)

    return numberStudents / numberGroups
}

const entries: number[][] = [
    [10, 9, 6, 8, 9, 1, 5, 7],
    [2, 5, 7, 1, -2],
    [10, 10, 10, 10, 9],
    [25, 75]
]

console.group(__filename.toUpperCase().split(/(\\|\/)/g).pop())
entries.forEach(group => {
    console.log(arithmeticAvarage(...group));
})
console.log('\n');
console.groupEnd()

export default {}