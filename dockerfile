FROM node:16
WORKDIR /ts-challenges
COPY . .
RUN npm i
ENTRYPOINT npm run dev